<?php

namespace Areaf5\CheckMigrationsCommand\Console;

use Illuminate\Console\Command;
use Illuminate\Database\Console\Migrations\StatusCommand;
use Illuminate\Support\Collection;

class CheckMigrationsCommand extends StatusCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrations:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if migrations are pending';


    /**
     * Fix constructor heredado
     */
    public function __construct()
    {
        $migrator = app("migrator");

        parent::__construct($migrator);

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return $this->migrator->usingConnection(null, function () {
            if (! $this->migrator->repositoryExists()) {
                $this->error('Migration table not found.');
                return Command::FAILURE;
            }

            $ran = $this->migrator->getRepository()->getRan();
            $batches = $this->migrator->getRepository()->getMigrationBatches();

            if (!$this->isMigrateRan($ran)) {
                $this->error('Migrations pending');
                return Command::FAILURE;
            }
            $this->info('No migrations found');
            return Command::SUCCESS;
        });
    }

    /**
     * Get the status for the given ran migrations.
     *
     * @param array $ran
     * @return bool
     */
    protected function isMigrateRan(array $ran): bool
    {
        return Collection::make($this->getAllMigrationFiles())
            ->every(function ($migration) use ($ran) {
                $migrationName = $this->migrator->getMigrationName($migration);
                return in_array($migrationName, $ran);
            });
    }

}
