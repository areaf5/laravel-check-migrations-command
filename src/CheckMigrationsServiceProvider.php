<?php

namespace Areaf5\CheckMigrationsCommand;

use Areaf5\CheckMigrationsCommand\Console\CheckMigrationsCommand;
use Illuminate\Support\ServiceProvider;

class CheckMigrationsServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        // Register the command if we are using the application via the CLI
        if ($this->app->runningInConsole()) {
            $this->commands([
                CheckMigrationsCommand::class,
            ]);
        }
    }
}
